import { Component, Input, OnInit } from '@angular/core';
import { Data } from '@angular/router';
import { TireData } from '../../models/data';
import { data } from '../mock-data';
import { TireDataService } from '../services/tiredata/tire-data.service';
import { SupabaseService } from '../services/supabase/supabase.service';

@Component({
  selector: 'app-saved-data',
  templateUrl: './saved-data.component.html',
  styleUrls: ['./saved-data.component.css']
})
export class SavedDataComponent implements OnInit {

  tires: TireData | null = null;
  tireData: any[];
  //public user = this.auth.currentUser;
  @Input() submittedData:Data;

  displayedColumns: string[] = ['date', 'lf', 'lr', 'rf', 'rr'];



  constructor(public dataService:TireDataService, private auth: SupabaseService) { }

  async ngOnInit() {

    this.tireData = await this.dataService.getAll();
    console.log(this.tireData);
  }


}
