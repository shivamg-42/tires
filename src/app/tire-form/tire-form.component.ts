import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TireData } from '../../models/data';
//import { data } from '../mock-data';
import { FormBuilder, FormControl, FormGroup, ReactiveFormsModule, NgForm, Validators } from '@angular/forms';
import { from } from 'rxjs';
import { formatDate } from '@angular/common';
import { TireDataService } from '../services/tiredata/tire-data.service';


@Component({
  selector: 'app-tire-form',
  templateUrl: './tire-form.component.html',
  styleUrls: ['./tire-form.component.css']
})
export class TireFormComponent implements OnInit {
  @Output() submittedData = new EventEmitter<TireData>();

  tireForm: FormGroup;
  currentDate = formatDate(new Date(), 'MM-dd-yyyy', 'en');

      date= "";
      lf: number;
      lr: number;
      rf: number;
      rr :number;


// https://www.concretepage.com/angular-2/angular-2-4-formbuilder-example

  getDate(){
    return this.tireForm.get('date')?.value;
  }
  onSubmit(data: TireData) {
    this.tireData.create(data);
    this.tireForm.reset();
  }

  constructor(private fb:FormBuilder, private tireData: TireDataService) {

    // this.tireForm = this.fb.group({
    //   date: [''],
    //   lf: [''],
    //   lr: [''],
    //   rf: [''],
    //   rr: [''],
    //   track: this.fb.group({
    //     name: [''],
    //     temperature: ['']
    //   }),
    // });

  }

  ngOnInit(): void {
    this.tireForm = this.fb.group({
      date: [formatDate(new Date(), 'MM-dd-yyyy', 'en'), Validators.required],
      Driver_Front: ['', [Validators.required, Validators.pattern("^[0-9]*$")]],
      Driver_Rear: ['', [Validators.required]],
      Pass_Front: ['', [Validators.required]],
      Pass_Rear: ['', [Validators.required]]
      // track: this.fb.group({
      //   name: [''],
      //   temperature: ['']
      // }),
    })
  }

  setValue(){
    this.date = this.tireForm.get('date')?.value;
    this.lf = this.tireForm.get('lf')?.value;
    this.lr = this.tireForm.get('lr')?.value;
    this.rf = this.tireForm.get('rf')?.value;
    this.rr = this.tireForm.get('rr')?.value;


  }



}
