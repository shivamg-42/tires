import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { SupabaseService } from '../services/supabase/supabase.service';
import { Subject } from 'rxjs';
import { Profile } from 'src/models/profile';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  public user: Subject<Profile>;
  constructor(private route: ActivatedRoute, private auth: SupabaseService ) {}

  ngOnInit(): void {

    this.user.subscribe();

    // this.route.queryParams.subscribe(params => {
    //   this.name = params['name'];
    // });
  }

  ngOnDestroy(): void{
    this.user.unsubscribe();
  }

}
