import { Injectable } from '@angular/core';
import {
  AuthChangeEvent,
  AuthSession,
  createClient,
  Session,
  SupabaseClient,
  User,
} from '@supabase/supabase-js'
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment'
import { Profile } from 'src/models/profile';



@Injectable({
  providedIn: 'root'
})


export class SupabaseService {

  private supabase: SupabaseClient;
  _session: AuthSession | null = null
  private currentUser: BehaviorSubject<boolean | User | any> = new BehaviorSubject(null)

  constructor() {
    this.supabase = createClient(environment.supabaseUrl, environment.supabaseKey)

    let user = this.supabase.auth.getUser()
    if (user) {
      this.currentUser.next(user)
    } else {
      this.currentUser.next(false)
    }
    }

  getSessions(){
    this.supabase.auth.getSession().then(({data}) => {
      this._session = data.session
    })
  }

  profile(user:User){
    return this.supabase.from('profiles').select('username,website,avatar_url').eq('id', user.id).single()
  }

  authChanges(callback: (event: AuthChangeEvent, session: Session | null) => void){
    return this.supabase.auth.onAuthStateChange(callback);
  }

  signIn(email: string){
    return this.supabase.auth.signInWithOtp({email});
  }

  signOut(){
    return this.supabase.auth.signOut();
  }

  updateProfile(profile: Profile){
    const update = {
      ...profile,
      updated_at: new Date(),
    }
    return this.supabase.from('profiles').upsert(update);
  }

  get activeUser(){
    return this.currentUser.asObservable();
  }
  // downLoadImage(path: string){
  //   return this.supabase.storage.from('avatars').download(path);
  // }

  // uploadAvatar(filePath: string, file: File){
  //   return this.supabase.storage.from('avatars').upload(filePath, file)
  // }

}
