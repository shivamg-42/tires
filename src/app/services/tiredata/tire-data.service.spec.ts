import { TestBed } from '@angular/core/testing';

import { TireDataService } from './tire-data.service';

describe('TireDataService', () => {
  let service: TireDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TireDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
