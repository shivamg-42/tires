import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs'
import { TireData } from '../../../models/data';
import { data } from '../../mock-data';
import { SupabaseClient, createClient } from '@supabase/supabase-js';
import { environment } from 'src/environments/environment';
import { SupabaseService } from '../supabase/supabase.service';


const baseUrl = 'http://localhost:8080/api/tires';
@Injectable({
  providedIn: 'root'
})
export class TireDataService {
  private supabase: SupabaseClient;

  tireData = data;

  constructor(private http: HttpClient, private auth: SupabaseService) {
    this.supabase = createClient(environment.supabaseUrl, environment.supabaseKey);
  }

  async getAll(){
    let tireData = await this.supabase.from('pressuredata').select();
    //return this.http.get<TireData[]>(baseUrl);
    return tireData.data || [];
  }

  get(id: any): Observable<any> {
    return this.http.get(`${baseUrl}/${id}`);
  }

  async create(data: any) {
    return this.supabase.from('pressuredata').insert(data);
  }

  update(id: any, data: any): Observable<any> {
    return this.http.put(`${baseUrl}/${id}`, data);
  }

  delete(id: any): Observable<any> {
    return this.http.delete(`${baseUrl}/${id}`);
  }

  deleteAll(): Observable<any> {
    return this.http.delete(baseUrl);
  }

  findByTitle(title: any): Observable<TireData[]> {
    return this.http.get<TireData[]>(`${baseUrl}?title=${title}`);
  }

}
