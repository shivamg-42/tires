export interface TireData{

    id: number;
    date: string;
    Driver_Front: number;
    Driver_Rear: number;
    Pass_Front: number;
    Pass_Rear: number;
}
